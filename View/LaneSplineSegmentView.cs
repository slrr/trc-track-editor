﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using SlrrLib.Model;
using SlrrLib.View;
using Vec3 = System.Windows.Media.Media3D.Vector3D;

namespace TrcTrackEditor.View
{
  public class LaneSplineSegmentView : NotifyPropertyChangedBase
  {
    private Trc parentTrc;
    private TrcLane connectedLane;
    private TrcTrackEditor.MainWindow parent;
    private HashSet<TrcIntersection> relatedIntersections = new HashSet<TrcIntersection>();

    public SplineSegmentView SplSegment
    {
      get;
      set;
    }

    public LaneSplineSegmentView(Trc trc, TrcLane paretnLane, TrcSplineSegment segment, TrcTrackEditor.MainWindow parentwond)
    {
      parentTrc = trc;
      connectedLane = paretnLane;
      SplSegment = new SplineSegmentView
      {
        managedSegment = segment
      };
      SplSegment.PropertyChanged += splSegment_PropertyChanged;
      parent = parentwond;
      ReCalcConnectedSegments();
    }

    public void ReCalcConnectedSegments()
    {
      SplSegment.mySrcPointToTheirSrcPoint.Clear();
      SplSegment.myTargetToTheirSrcPoint.Clear();
      SplSegment.mySrcPointToTheirTarget.Clear();
      SplSegment.myTargetToTheirTarget.Clear();
      relatedIntersections.Clear();
      parentTrc.RecalcConnectionsForLane(connectedLane);
      double maxDistToBeTheSame = 0.5;
      if (SplSegment.managedSegment == connectedLane.Spline.First())
      {
        foreach (var cross in parentTrc.IntersectionData1.Union(parentTrc.IntersectionData2))
        {
          foreach (var spl in cross.Crossings)
          {
            if ((spl.LaneShape.TargetLanePosition - SplSegment.managedSegment.TargetLanePosition).Length < maxDistToBeTheSame)
            {
              relatedIntersections.Add(cross);
              SplSegment.myTargetToTheirTarget.Add(spl.LaneShape);
            }
            else if ((spl.LaneShape.SourceLanePosition - SplSegment.managedSegment.TargetLanePosition).Length < maxDistToBeTheSame)
            {
              relatedIntersections.Add(cross);
              SplSegment.myTargetToTheirSrcPoint.Add(spl.LaneShape);
            }
          }
        }
      }
      if (SplSegment.managedSegment == connectedLane.Spline.Last())
      {
        foreach (var cross in parentTrc.IntersectionData1.Union(parentTrc.IntersectionData2))
        {
          foreach (var spl in cross.Crossings)
          {
            if ((spl.LaneShape.TargetLanePosition - SplSegment.managedSegment.SourceLanePosition).Length < maxDistToBeTheSame)
            {
              relatedIntersections.Add(cross);
              SplSegment.mySrcPointToTheirTarget.Add(spl.LaneShape);
            }
            else if ((spl.LaneShape.SourceLanePosition - SplSegment.managedSegment.SourceLanePosition).Length < maxDistToBeTheSame)
            {
              relatedIntersections.Add(cross);
              SplSegment.mySrcPointToTheirSrcPoint.Add(spl.LaneShape);
            }
          }
        }
      }
      foreach (var laneSpl in connectedLane.Spline)
      {
        if (laneSpl == SplSegment.managedSegment)
          continue;
        if ((laneSpl.SourceLanePosition - SplSegment.SourcePosition).Length < maxDistToBeTheSame)
        {
          SplSegment.mySrcPointToTheirSrcPoint.Add(laneSpl);
        }
        else if ((laneSpl.TargetLanePosition - SplSegment.SourcePosition).Length < maxDistToBeTheSame)
        {
          SplSegment.mySrcPointToTheirTarget.Add(laneSpl);
        }
        else if ((laneSpl.SourceLanePosition - SplSegment.TargetPosition).Length < maxDistToBeTheSame)
        {
          SplSegment.myTargetToTheirSrcPoint.Add(laneSpl);
        }
        else if ((laneSpl.TargetLanePosition - SplSegment.TargetPosition).Length < maxDistToBeTheSame)
        {
          SplSegment.myTargetToTheirTarget.Add(laneSpl);
        }
      }
    }
    public void UpdateRelatedVisuals()
    {
      parent.TrcGeom.UpdateGeneratedVisualsOf(connectedLane);
      foreach (var cross in relatedIntersections)
        parent.TrcGeom.UpdateGeneratedVisualsOf(cross);
    }
    public void SnapSourceToMarker()
    {
      SplSegment.SourcePosition = parent.MarkerPositionInRpkSpace;
    }
    public void SnapTargetToMarker()
    {
      SplSegment.TargetPosition = parent.MarkerPositionInRpkSpace;
    }
    public void SnapSourceControlPToMarker()
    {
      SplSegment.SourceControlP = (parent.MarkerPositionInRpkSpace - SplSegment.SourcePosition) * -SlrrLib.Model.Trc.DividerForBezier;
    }
    public void SnapTargetControlPToMarker()
    {
      SplSegment.TargetControlP = (parent.MarkerPositionInRpkSpace - SplSegment.TargetPosition) * SlrrLib.Model.Trc.DividerForBezier;
    }
    public void SnapSourcePointToFirstClosePoint(double maxDist = 2)
    {
      foreach (var intersect in parentTrc.IntersectionData1.Union(parentTrc.IntersectionData2))
      {
        foreach (var crossLane in intersect.Crossings)
        {
          if (crossLane.LaneShape == SplSegment.managedSegment)
            continue;
          if ((SplSegment.SourcePosition - crossLane.LaneShape.SourceLanePosition).Length < maxDist)
          {
            SplSegment.SourcePosition = crossLane.LaneShape.SourceLanePosition;
            SplSegment.SourceControlP = crossLane.LaneShape.SourceLaneMinusDeltaControlP;
            ReCalcConnectedSegments();
            return;
          }
          if ((SplSegment.SourcePosition - crossLane.LaneShape.TargetLanePosition).Length < maxDist)
          {
            SplSegment.SourcePosition = crossLane.LaneShape.TargetLanePosition;
            SplSegment.SourceControlP = crossLane.LaneShape.TargetLaneDeltaControlPoint;
            ReCalcConnectedSegments();
            return;
          }
        }
      }
    }
    public void SnapTargetPointToFirstClosePoint(double maxDist = 2)
    {
      foreach (var intersect in parentTrc.IntersectionData1.Union(parentTrc.IntersectionData2))
      {
        foreach (var crossLane in intersect.Crossings)
        {
          if (crossLane.LaneShape == SplSegment.managedSegment)
            continue;
          if ((SplSegment.TargetPosition - crossLane.LaneShape.SourceLanePosition).Length < maxDist)
          {
            SplSegment.TargetPosition = crossLane.LaneShape.SourceLanePosition;
            SplSegment.TargetControlP = crossLane.LaneShape.SourceLaneMinusDeltaControlP;
            ReCalcConnectedSegments();
            return;
          }
          if ((SplSegment.TargetPosition - crossLane.LaneShape.TargetLanePosition).Length < maxDist)
          {
            SplSegment.TargetPosition = crossLane.LaneShape.TargetLanePosition;
            SplSegment.TargetControlP = crossLane.LaneShape.TargetLaneDeltaControlPoint;
            ReCalcConnectedSegments();
            return;
          }
        }
      }
    }

    private void splSegment_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      UpdateRelatedVisuals();
    }
  }
}
