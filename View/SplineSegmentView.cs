﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using SlrrLib.Model;
using SlrrLib.View;
using Vec3 = System.Windows.Media.Media3D.Vector3D;

namespace TrcTrackEditor.View
{
  public class SplineSegmentView : NotifyPropertyChangedBase
  {
    public List<TrcSplineSegment> mySrcPointToTheirSrcPoint = new List<TrcSplineSegment>();
    public List<TrcSplineSegment> myTargetToTheirSrcPoint = new List<TrcSplineSegment>();
    public List<TrcSplineSegment> myTargetToTheirTarget = new List<TrcSplineSegment>();
    public List<TrcSplineSegment> mySrcPointToTheirTarget = new List<TrcSplineSegment>();
    public TrcSplineSegment managedSegment;

    public string SourceSpeedRatio
    {
      get
      {
        return managedSegment.LengthOfSplineAtSource.ToString();
      }
      set
      {
        if (SlrrLib.View.UIUtil.ParseOrFalse(value, out float val))
        {
          managedSegment.LengthOfSplineAtSource = val;
          OnPropertyChanged("SourceSpeedRatio");
        }
      }
    }
    public string TargetSpeedRatio
    {
      get
      {
        return managedSegment.LengthOfSplineAtTarget.ToString();
      }
      set
      {
        if (SlrrLib.View.UIUtil.ParseOrFalse(value, out float val))
        {
          managedSegment.LengthOfSplineAtTarget = val;
          OnPropertyChanged("TargetSpeedRatio");
        }
      }
    }
    public Vec3 SourcePosition
    {
      get
      {
        return managedSegment.SourceLanePosition;
      }
      set
      {
        managedSegment.SourceLanePosition = value;
        foreach (var other in mySrcPointToTheirSrcPoint)
          other.SourceLanePosition = value;
        foreach (var other in mySrcPointToTheirTarget)
          other.TargetLanePosition = value;
        OnPropertyChanged("SourceSpeedRatio");
      }
    }
    public Vec3 SourceControlP
    {
      get
      {
        return managedSegment.SourceLaneMinusDeltaControlP;
      }
      set
      {
        managedSegment.SourceLaneMinusDeltaControlP = value;
        var valNorm = value;
        valNorm.Normalize();
        foreach (var other in mySrcPointToTheirSrcPoint)
        {
          other.SourceLaneMinusDeltaControlP = valNorm * other.SourceLaneMinusDeltaControlP.Length;
        }
        foreach (var other in mySrcPointToTheirTarget)
        {
          other.TargetLaneDeltaControlPoint = valNorm * other.TargetLaneDeltaControlPoint.Length;
        }
        OnPropertyChanged("SourceSpeedRatio");
      }
    }
    public Vec3 TargetPosition
    {
      get
      {
        return managedSegment.TargetLanePosition;
      }
      set
      {
        managedSegment.TargetLanePosition = value;
        foreach (var other in myTargetToTheirSrcPoint)
          other.SourceLanePosition = value;
        foreach (var other in myTargetToTheirTarget)
          other.TargetLanePosition = value;
        OnPropertyChanged("SourceSpeedRatio");
      }
    }
    public Vec3 TargetControlP
    {
      get
      {
        return managedSegment.TargetLaneDeltaControlPoint;
      }
      set
      {
        managedSegment.TargetLaneDeltaControlPoint = value;
        var valNorm = value;
        valNorm.Normalize();
        foreach (var other in myTargetToTheirSrcPoint)
        {
          other.SourceLaneMinusDeltaControlP = valNorm * other.SourceLaneMinusDeltaControlP.Length;
        }
        foreach (var other in myTargetToTheirTarget)
        {
          other.TargetLaneDeltaControlPoint = valNorm * other.TargetLaneDeltaControlPoint.Length;
        }
        OnPropertyChanged("SourceSpeedRatio");
      }
    }
  }
}
