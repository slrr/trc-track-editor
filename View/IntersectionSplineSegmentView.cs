﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using SlrrLib.Model;
using SlrrLib.View;
using Vec3 = System.Windows.Media.Media3D.Vector3D;

namespace TrcTrackEditor.View
{
  public class IntersectionSplineSegmentView : NotifyPropertyChangedBase
  {
    private string strSuffix = "";

    private Trc parentTrc;
    private TrcIntersection connectedIntersection;
    private TrcTrackEditor.MainWindow parent;

    public SplineSegmentView SplSegment
    {
      get;
      set;
    }
    public TrcIntersectionLane ManagedLane
    {
      get;
      set;
    }
    public string StrRepresentation
    {
      get
      {
        int found = 0;
        foreach (var crossing in connectedIntersection.Crossings)
        {
          if (crossing.LaneShape == SplSegment.managedSegment)
          {
            break;
          }
          found++;
        }
        return "CrossingShape " + found.ToString("D2") + " " + strSuffix;
      }
    }

    public IntersectionSplineSegmentView(Trc trc, TrcIntersection paretnCross, TrcIntersectionLane crossLane, TrcTrackEditor.MainWindow parentwond, string suffix = "")
    {
      parentTrc = trc;
      connectedIntersection = paretnCross;
      SplSegment = new SplineSegmentView
      {
        managedSegment = crossLane.LaneShape
      };
      SplSegment.PropertyChanged += splSegment_PropertyChanged;
      ManagedLane = crossLane;
      parent = parentwond;
      ReCalcConnectedSegments();
      strSuffix = suffix;
    }

    public void ReCalcConnectedSegments()
    {
      SplSegment.mySrcPointToTheirSrcPoint.Clear();
      SplSegment.myTargetToTheirSrcPoint.Clear();
      SplSegment.mySrcPointToTheirTarget.Clear();
      SplSegment.myTargetToTheirTarget.Clear();
      double maxDistToBeTheSame = 0.5;
      parentTrc.RecalcConnectionsForIntersection(connectedIntersection);
      foreach (var spl in connectedIntersection.Crossings)
      {
        if (spl.LaneShape == SplSegment.managedSegment)
          continue;
        if ((spl.LaneShape.SourceLanePosition - SplSegment.managedSegment.SourceLanePosition).Length < maxDistToBeTheSame)
        {
          SplSegment.mySrcPointToTheirSrcPoint.Add(spl.LaneShape);
        }
        else if ((spl.LaneShape.SourceLanePosition - SplSegment.managedSegment.TargetLanePosition).Length < maxDistToBeTheSame)
        {
          SplSegment.myTargetToTheirSrcPoint.Add(spl.LaneShape);
        }
        else if ((spl.LaneShape.TargetLanePosition - SplSegment.managedSegment.SourceLanePosition).Length < maxDistToBeTheSame)
        {
          SplSegment.mySrcPointToTheirTarget.Add(spl.LaneShape);
        }
        else if ((spl.LaneShape.TargetLanePosition - SplSegment.managedSegment.TargetLanePosition).Length < maxDistToBeTheSame)
        {
          SplSegment.myTargetToTheirTarget.Add(spl.LaneShape);
        }
      }
      foreach (var lane in connectedIntersection.LaneRefs)
      {
        if ((lane.LaneInFlattened_1.EndPoint - SplSegment.managedSegment.TargetLanePosition).Length < maxDistToBeTheSame)
        {
          SplSegment.myTargetToTheirTarget.Add(lane.LaneInFlattened_1.Spline.First());
        }
        else if ((lane.LaneInFlattened_1.EndPoint - SplSegment.managedSegment.SourceLanePosition).Length < maxDistToBeTheSame)
        {
          SplSegment.mySrcPointToTheirTarget.Add(lane.LaneInFlattened_1.Spline.First());
        }
        if ((lane.LaneInFlattened_1.StartPoint - SplSegment.managedSegment.TargetLanePosition).Length < maxDistToBeTheSame)
        {
          SplSegment.myTargetToTheirSrcPoint.Add(lane.LaneInFlattened_1.Spline.Last());
        }
        else if ((lane.LaneInFlattened_1.StartPoint - SplSegment.managedSegment.SourceLanePosition).Length < maxDistToBeTheSame)
        {
          SplSegment.mySrcPointToTheirSrcPoint.Add(lane.LaneInFlattened_1.Spline.Last());
        }
        else
        {
          continue; // !
        }
      }
    }
    public void UpdateRelatedVisuals()
    {
      parent.TrcGeom.UpdateGeneratedVisualsOf(connectedIntersection);
      foreach (var lane in connectedIntersection.LaneRefs)
        parent.TrcGeom.UpdateGeneratedVisualsOf(lane.LaneInFlattened_1);
    }
    public void SnapSourceToMarker()
    {
      SplSegment.SourcePosition = parent.MarkerPositionInRpkSpace;
    }
    public void SnapTargetToMarker()
    {
      SplSegment.TargetPosition = parent.MarkerPositionInRpkSpace;
    }
    public void SnapSourceControlPToMarker()
    {
      SplSegment.SourceControlP = (parent.MarkerPositionInRpkSpace - SplSegment.SourcePosition) * -SlrrLib.Model.Trc.DividerForBezier;
    }
    public void SnapTargetControlPToMarker()
    {
      SplSegment.TargetControlP = (parent.MarkerPositionInRpkSpace - SplSegment.TargetPosition) * SlrrLib.Model.Trc.DividerForBezier;
    }
    public void SnapSourcePointToFirstClosePoint(double maxDist = 2)
    {
      foreach (var intersect in parentTrc.IntersectionData1.Union(parentTrc.IntersectionData2))
      {
        foreach (var crossLane in intersect.Crossings)
        {
          if (crossLane.LaneShape == SplSegment.managedSegment)
            continue;
          if ((SplSegment.SourcePosition - crossLane.LaneShape.SourceLanePosition).Length < maxDist)
          {
            SplSegment.SourcePosition = crossLane.LaneShape.SourceLanePosition;
            SplSegment.SourceControlP = crossLane.LaneShape.SourceLaneMinusDeltaControlP;
            ReCalcConnectedSegments();
            return;
          }
          if ((SplSegment.SourcePosition - crossLane.LaneShape.TargetLanePosition).Length < maxDist)
          {
            SplSegment.SourcePosition = crossLane.LaneShape.TargetLanePosition;
            SplSegment.SourceControlP = crossLane.LaneShape.TargetLaneDeltaControlPoint;
            ReCalcConnectedSegments();
            return;
          }
        }
      }
      foreach (var lane in parentTrc.LaneData)
      {
        if ((SplSegment.SourcePosition - lane.EndPoint).Length < maxDist)
        {
          SplSegment.SourcePosition = lane.EndPoint;
          SplSegment.SourceControlP = lane.EndNormal;
          ReCalcConnectedSegments();
          return;
        }
      }
    }
    public void SnapTargetPointToFirstClosePoint(double maxDist = 2)
    {
      foreach (var intersect in parentTrc.IntersectionData1.Union(parentTrc.IntersectionData2))
      {
        foreach (var crossLane in intersect.Crossings)
        {
          if (crossLane.LaneShape == SplSegment.managedSegment)
            continue;
          if ((SplSegment.TargetPosition - crossLane.LaneShape.SourceLanePosition).Length < maxDist)
          {
            SplSegment.TargetPosition = crossLane.LaneShape.SourceLanePosition;
            SplSegment.TargetControlP = crossLane.LaneShape.SourceLaneMinusDeltaControlP;
            ReCalcConnectedSegments();
            return;
          }
          if ((SplSegment.TargetPosition - crossLane.LaneShape.TargetLanePosition).Length < maxDist)
          {
            SplSegment.TargetPosition = crossLane.LaneShape.TargetLanePosition;
            SplSegment.TargetControlP = crossLane.LaneShape.TargetLaneDeltaControlPoint;
            ReCalcConnectedSegments();
            return;
          }
        }
      }
      foreach (var lane in parentTrc.LaneData)
      {
        if ((SplSegment.TargetPosition - lane.StartPoint).Length < maxDist)
        {
          SplSegment.TargetPosition = lane.StartPoint;
          SplSegment.TargetControlP = lane.StartNormal;
          ReCalcConnectedSegments();
          return;
        }
      }
    }

    private void splSegment_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      UpdateRelatedVisuals();
    }
  }
}
