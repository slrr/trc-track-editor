﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SlrrLib.Model;
using TrcTrackEditor.View;

namespace TrcTrackEditor
{
  public partial class TrcLaneEditor : UserControl
  {
    private Trc trc;
    private TrcLane lane;
    private TrcTrackEditor.MainWindow parent;

    public TrcLaneEditor()
    {
      InitializeComponent();
    }

    public void ConnectLane(TrcLane toManage, Trc parentTrc, TrcTrackEditor.MainWindow parentWnd)
    {
      trc = parentTrc;
      parent = parentWnd;
      lane = toManage;
      ctrlListBoxLaneSegments.Items.Clear();
      foreach (var segment in lane.Spline)
      {
        ctrlListBoxLaneSegments.Items.Add(segment);
      }
      if (ctrlLaneEditor.MainWindowParent == null)
      {
        ctrlLaneEditor.MainWindowParent = parentWnd;
        ctrlLaneEditor.SubscribeToMarkerMovement();
      }
      ctrlComboConnectedLaneDrop1.Items.Clear();
      ctrlComboConnectedLaneDrop2.Items.Clear();
      if (lane.LeftAdjacentLane != null)
      {
        ctrlComboConnectedLaneDrop1.Items.Add(lane.LeftAdjacentLane);
        ctrlComboConnectedLaneDrop1.SelectedItem = lane.LeftAdjacentLane;
      }
      if (lane.RightAdjacentLane != null)
      {
        ctrlComboConnectedLaneDrop2.Items.Add(lane.RightAdjacentLane);
        ctrlComboConnectedLaneDrop2.SelectedItem = lane.RightAdjacentLane;
      }
      ctrlTextBoxConnectedEndFloat1.Text = toManage.MaxSpeed.ToString("F4", System.Globalization.CultureInfo.InvariantCulture);
      ctrlTextBoxConnectedEndFloat2.Text = toManage.EndFloatData_2.ToString("F4", System.Globalization.CultureInfo.InvariantCulture);
    }
    public void ClearListbox()
    {
      ctrlListBoxLaneSegments.Items.Clear();
    }
    public void ClearSelection()
    {
      ctrlListBoxLaneSegments.SelectedIndex = -1;
      if (parent != null)
        parent.UnLockSelection();
    }

    private void ctrlListBoxCrossings_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (e.AddedItems.Count != 0)
      {
        var item = e.AddedItems[0] as TrcSplineSegment;
        parent.LockSelection();
        ctrlLaneEditor.SetCurrentSegmentView(new LaneSplineSegmentView(trc, lane, item, parent));
      }
      else
      {
        ctrlLaneEditor.DisableInteractions();
        ClearSelection();
      }
    }
    private void ctrlButtonClearSelection_Click(object sender, RoutedEventArgs e)
    {
      ClearSelection();
    }
    private void ctrlButtonAddPointToStart_Click(object sender, RoutedEventArgs e)
    {
      TrcSplineSegment toad = new TrcSplineSegment
      {
        TargetLanePosition = lane.StartPoint,
        TargetLaneDeltaControlPoint = lane.StartNormal
      };
      var lastSegment = lane.Spline.Last();
      toad.SourceLaneMinusDeltaControlP = toad.TargetLaneDeltaControlPoint;
      var addition = -(lastSegment.TargetLanePosition - lastSegment.SourceLanePosition);
      addition.Normalize();
      addition *= 10;
      toad.SourceLanePosition = toad.TargetLanePosition + addition;
      toad.LengthOfSplineAtSource = lastSegment.LengthOfSplineAtSource;
      toad.LengthOfSplineAtTarget = lastSegment.LengthOfSplineAtSource;
      toad.UnkByte_1 = 1;
      lane.Spline.Add(toad);
      ctrlListBoxLaneSegments.Items.Add(toad);
      ctrlListBoxLaneSegments.SelectedIndex = ctrlListBoxLaneSegments.Items.Count - 1;
      ctrlLaneEditor.CurrentSegmentView.UpdateRelatedVisuals();
    }
    private void ctrlButtonAddPointToEnd_Click(object sender, RoutedEventArgs e)
    {
      TrcSplineSegment toad = new TrcSplineSegment
      {
        SourceLanePosition = lane.EndPoint,
        SourceLaneMinusDeltaControlP = lane.EndNormal
      };
      var firstSegment = lane.Spline.First();
      toad.TargetLaneDeltaControlPoint = toad.SourceLaneMinusDeltaControlP;
      var addition = -(firstSegment.SourceLanePosition - firstSegment.TargetLanePosition);
      addition.Normalize();
      addition *= 10;
      toad.TargetLanePosition = toad.SourceLanePosition + addition;
      toad.LengthOfSplineAtSource = firstSegment.LengthOfSplineAtTarget;
      toad.LengthOfSplineAtTarget = firstSegment.LengthOfSplineAtTarget;
      toad.UnkByte_1 = 1;
      lane.Spline.Insert(0, toad);
      ctrlListBoxLaneSegments.Items.Insert(0, toad);
      ctrlListBoxLaneSegments.SelectedIndex = 0;
      ctrlLaneEditor.CurrentSegmentView.UpdateRelatedVisuals();
    }
    private void ctrlButtonDelSegment_Click(object sender, RoutedEventArgs e)
    {
      int prevSelect = ctrlListBoxLaneSegments.SelectedIndex;
      var item = ctrlListBoxLaneSegments.SelectedItem as TrcSplineSegment;
      lane.Spline.Remove(item);
      ctrlListBoxLaneSegments.SelectedIndex = -1;
      ctrlListBoxLaneSegments.Items.RemoveAt(prevSelect);
      ctrlLaneEditor.CurrentSegmentView.UpdateRelatedVisuals();
    }
    private void ctrlComboConnectedLaneDrop1_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (e.AddedItems.Count > 0)
      {
        lane.LeftAdjacentLane = e.AddedItems[0] as TrcLane;
        parent.TrcGeom.HighLightChildrenEntititesClearBefore(lane);
      }
    }
    private void ctrlComboConnectedLaneDrop2_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (e.AddedItems.Count > 0)
      {
        lane.RightAdjacentLane = e.AddedItems[0] as TrcLane;
        parent.TrcGeom.HighLightChildrenEntititesClearBefore(lane);
      }
    }
    private void ctrlButtonGenerateNeighboringLanes_Click(object sender, RoutedEventArgs e)
    {
      trc.RecalcConnectionsForLane(lane);
      trc.GenerateAdjacentLanesForLane(lane);
      parent.TrcGeom.HighLightChildrenEntititesClearBefore(lane);
    }
    private void ctrlComboConnectedLaneDrop1_DropDownOpened(object sender, EventArgs e)
    {
      var selected = ctrlComboConnectedLaneDrop1.SelectedItem as TrcLane;
      ctrlComboConnectedLaneDrop1.Items.Clear();
      ctrlComboConnectedLaneDrop1.Items.Add(null);
      trc.RecalcConnectionsForLane(lane);
      var crosses = trc.GetRelatedIntersections(lane);
      foreach (var cross in crosses)
      {
        foreach (var laneRef in cross.LaneRefs)
        {
          if (laneRef.LaneInFlattened_1 == lane)
            continue;
          if (ctrlComboConnectedLaneDrop1.Items.Contains(laneRef.LaneInFlattened_1))
            continue;
          ctrlComboConnectedLaneDrop1.Items.Add(laneRef.LaneInFlattened_1);
        }
      }
      if (selected != null && !ctrlComboConnectedLaneDrop1.Items.Contains(selected))
        ctrlComboConnectedLaneDrop1.Items.Insert(0, selected);
      if (selected != null)
        ctrlComboConnectedLaneDrop1.SelectedItem = selected;
    }
    private void ctrlComboConnectedLaneDrop2_DropDownOpened(object sender, EventArgs e)
    {
      var selected = ctrlComboConnectedLaneDrop2.SelectedItem as TrcLane;
      ctrlComboConnectedLaneDrop2.Items.Clear();
      ctrlComboConnectedLaneDrop2.Items.Add(null);
      trc.RecalcConnectionsForLane(lane);
      var crosses = trc.GetRelatedIntersections(lane);
      foreach (var cross in crosses)
      {
        foreach (var laneRef in cross.LaneRefs)
        {
          if (laneRef.LaneInFlattened_1 == lane)
            continue;
          if (ctrlComboConnectedLaneDrop2.Items.Contains(laneRef.LaneInFlattened_1))
            continue;
          ctrlComboConnectedLaneDrop2.Items.Add(laneRef.LaneInFlattened_1);
        }
      }
      if (selected != null && !ctrlComboConnectedLaneDrop2.Items.Contains(selected))
        ctrlComboConnectedLaneDrop2.Items.Insert(0, selected);
      if (selected != null)
        ctrlComboConnectedLaneDrop2.SelectedItem = selected;
    }
    private void ctrlTextBoxConnectedEndFloat1_KeyUp(object sender, KeyEventArgs e)
    {
      try
      {
        lane.MaxSpeed = float.Parse(ctrlTextBoxConnectedEndFloat1.Text, System.Globalization.CultureInfo.InvariantCulture);
        ctrlLaneEditor.CurrentSegmentView.UpdateRelatedVisuals();
      }
      catch (Exception) { }
    }
    private void ctrlTextBoxConnectedEndFloat2_KeyUp(object sender, KeyEventArgs e)
    {
      try
      {
        lane.EndFloatData_2 = float.Parse(ctrlTextBoxConnectedEndFloat2.Text, System.Globalization.CultureInfo.InvariantCulture);
      }
      catch (Exception) { }
    }
    private void ctrlButtonReverseSpline_Click(object sender, RoutedEventArgs e)
    {
      lane.Spline.Reverse();
      ctrlListBoxLaneSegments.Items.Clear();
      foreach (var spl in lane.Spline)
      {
        var bak_p = spl.TargetLanePosition;
        var bak_n = spl.TargetLaneDeltaControlPoint;
        spl.TargetLanePosition = spl.SourceLanePosition;
        spl.TargetLaneDeltaControlPoint = spl.SourceLaneMinusDeltaControlP;
        spl.SourceLanePosition = bak_p;
        spl.SourceLaneMinusDeltaControlP = bak_n;
        ctrlListBoxLaneSegments.Items.Add(spl);
      }
    }
    private void ctrlButtonSplitSegment_Click(object sender, RoutedEventArgs e)
    {
      float t = 0.5f;
      try
      {
        t = float.Parse(ctrlTextBoxSplitSegmentParam.Text, System.Globalization.CultureInfo.InvariantCulture);
      }
      catch (Exception)
      {
        return;
      }

      if (!(ctrlListBoxLaneSegments.SelectedItem is TrcSplineSegment spl))
        return;
      int indexOfSpl = lane.Spline.IndexOf(spl);
      var newP = spl.EvaluateAtParam(t);
      var toadSpl = new TrcSplineSegment
      {
        UnkByte_1 = 1,
        SourceLanePosition = spl.SourceLanePosition,
        SourceLaneMinusDeltaControlP = spl.SourceLaneMinusDeltaControlP,
        TargetLanePosition = newP
      };
      var normal = spl.SourceLanePosition - spl.TargetLanePosition;
      normal.Normalize();
      toadSpl.TargetLaneDeltaControlPoint = normal;
      spl.SourceLanePosition = newP;
      spl.SourceLaneMinusDeltaControlP = normal;
      lane.Spline.Insert(indexOfSpl + 1, toadSpl);
      ctrlListBoxLaneSegments.Items.Insert(indexOfSpl + 1, toadSpl);
      ctrlLaneEditor.CurrentSegmentView.UpdateRelatedVisuals();
    }
  }
}
