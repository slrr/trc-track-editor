﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SlrrLib.Model;
using TrcTrackEditor.View;

namespace TrcTrackEditor
{
  public partial class TrcIntersectionEditor : UserControl
  {
    private Trc trc;
    private TrcIntersection cross;
    private TrcTrackEditor.MainWindow parent;

    public TrcIntersectionEditor()
    {
      InitializeComponent();
    }

    public void ConnectIntersection(TrcIntersection intersection, Trc parentTrc, TrcTrackEditor.MainWindow parentWnd)
    {
      trc = parentTrc;
      parent = parentWnd;
      cross = intersection;
      ctrlListBoxCrossings.Items.Clear();
      var isType1Intersection = trc.IntersectionData1.Contains(intersection);
      if (isType1Intersection)
        ctrlSwitchIntersectionType.Content = "Switch Type (Current:RED)";
      else
        ctrlSwitchIntersectionType.Content = "Switch Type (Current:GREEN)";
      foreach (var crossLanes in cross.Crossings)
      {
        ctrlListBoxCrossings.Items.Add(crossLanes);
      }
      if (ctrlCrossingLaneEditor.MainWindowParent == null)
      {
        ctrlCrossingLaneEditor.MainWindowParent = parentWnd;
        ctrlCrossingLaneEditor.SubscribeToMarkerMovement();
      }
    }
    public void ClearListbox()
    {
      ctrlListBoxCrossings.Items.Clear();
    }
    public void ClearSelection()
    {
      ctrlListBoxCrossings.SelectedIndex = -1;
      if (parent != null)
        parent.UnLockSelection();
    }
    public void AddNewLane()
    {
      TrcIntersectionLane toad = new TrcIntersectionLane
      {
        LaneShape = new TrcSplineSegment
        {
          SourceLanePosition = parent.MarkerPositionInRpkSpace
        }
      };
      toad.LaneShape.TargetLanePosition = toad.LaneShape.SourceLanePosition + new System.Windows.Media.Media3D.Vector3D(-10, 0, 0);
      toad.LaneShape.SourceLaneMinusDeltaControlP = new System.Windows.Media.Media3D.Vector3D(1, 0, 0);
      toad.LaneShape.TargetLaneDeltaControlPoint = new System.Windows.Media.Media3D.Vector3D(1, 0, 0);
      toad.LaneShape.LengthOfSplineAtSource = 10;
      toad.LaneShape.LengthOfSplineAtTarget = 10;
      toad.LaneShape.UnkByte_1 = 1;
      cross.Crossings.Add(toad);
      ctrlListBoxCrossings.Items.Add(toad);
      ctrlListBoxCrossings.SelectedIndex = ctrlListBoxCrossings.Items.Count - 1;
      ctrlCrossingLaneEditor.CurrentSegmentView.UpdateRelatedVisuals();
    }

    private void ctrlListBoxCrossings_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (e.AddedItems.Count != 0)
      {
        var item = e.AddedItems[0] as TrcIntersectionLane;
        parent.LockSelection();
        ctrlCrossingLaneEditor.SetCurrentSegmentView(new IntersectionSplineSegmentView(trc, cross, item, parent));
      }
      else
      {
        ctrlCrossingLaneEditor.DisableInteractions();
        ClearSelection();
      }
    }
    private void ctrlButtonClearSelection_Click(object sender, RoutedEventArgs e)
    {
      ClearSelection();
    }
    private void ctrlButtonAddNewLane_Click(object sender, RoutedEventArgs e)
    {
      AddNewLane();
    }
    private void ctrlButtonDelLane_Click(object sender, RoutedEventArgs e)
    {
      int prevSelect = ctrlListBoxCrossings.SelectedIndex;
      var item = ctrlListBoxCrossings.SelectedItem as TrcIntersectionLane;
      cross.Crossings.Remove(item);
      ctrlListBoxCrossings.SelectedIndex = -1;
      ctrlListBoxCrossings.Items.RemoveAt(prevSelect);
      ctrlCrossingLaneEditor.CurrentSegmentView.UpdateRelatedVisuals();
    }
    private void ctrlSwitchIntersectionType_Click(object sender, RoutedEventArgs e)
    {
      var isType1Intersection = trc.IntersectionData1.Contains(cross);
      if (isType1Intersection)
      {
        trc.IntersectionData1.Remove(cross);
        trc.IntersectionData2.Add(cross);
        ctrlSwitchIntersectionType.Content = "Switch Type (Current:RED)";
      }
      else
      {
        trc.IntersectionData2.Remove(cross);
        trc.IntersectionData1.Add(cross);
        ctrlSwitchIntersectionType.Content = "Switch Type (Current:GREEN)";
      }
    }
  }
}
