﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TrcTrackEditor.View;

namespace TrcTrackEditor
{
  public partial class LaneEditor : UserControl
  {
    private bool disabled = true;

    public MainWindow MainWindowParent
    {
      get;
      set;
    }
    public LaneSplineSegmentView CurrentSegmentView
    {
      get;
      set;
    }

    public LaneEditor()
    {
      InitializeComponent();
    }
    public void DisableInteractions()
    {
      disabled = true;
      //ctrlTextBoxSourceSpeed.Text = "";
      //ctrlTextBoxTargetSpeed.Text = "";
    }
    public void SetCurrentSegmentView(LaneSplineSegmentView newView)
    {
      disabled = false;
      CurrentSegmentView = newView;
      //ctrlTextBoxSourceSpeed.Text = currentSegmentView.splSegment.SourceSpeedRatio;
      //ctrlTextBoxTargetSpeed.Text = currentSegmentView.splSegment.TargetSpeedRatio;
      updateMarkerPosition();
      CurrentSegmentView.UpdateRelatedVisuals();
    }
    public void SubscribeToMarkerMovement()
    {
      MainWindowParent.MarkerPositionChangedByUser += ctrlMainWindowParent_MarkerPositionChangedByUser;
    }

    private void updateMarkerPosition()
    {
      if (disabled)
        return;
      if (ctrlListBoxSrcOrTarget.SelectedIndex == 0) //MoveSource
      {
        MainWindowParent.MarkerPosition = CurrentSegmentView.SplSegment.SourcePosition;
      }
      else
      {
        MainWindowParent.MarkerPosition = CurrentSegmentView.SplSegment.TargetPosition;
      }
    }

    private void ctrlMainWindowParent_MarkerPositionChangedByUser(object sender, EventArgs e)
    {
      if (disabled)
        return;
      if (ctrlListBoxSrcOrTarget.SelectedIndex == 0) //MoveSource
      {
        CurrentSegmentView.SnapSourceToMarker();
      }
      else if (ctrlListBoxSrcOrTarget.SelectedIndex == 1)
      {
        CurrentSegmentView.SnapTargetToMarker();
      }
      else if (ctrlListBoxSrcOrTarget.SelectedIndex == 2) //MoveSourceNormal
      {
        CurrentSegmentView.SnapSourceControlPToMarker();
      }
      else if (ctrlListBoxSrcOrTarget.SelectedIndex == 3)
      {
        CurrentSegmentView.SnapTargetControlPToMarker();
      }
    }
    private void ctrlButtonSnap_Click(object sender, RoutedEventArgs e)
    {
      if (disabled)
        return;
      if (ctrlListBoxSrcOrTarget.SelectedIndex == 0) //MoveSource
      {
        CurrentSegmentView.SnapSourcePointToFirstClosePoint();
      }
      else if (ctrlListBoxSrcOrTarget.SelectedIndex == 1)
      {
        CurrentSegmentView.SnapTargetPointToFirstClosePoint();
      }
      updateMarkerPosition();
    }
    private void ctrlButtonRecalcSnapping_Click(object sender, RoutedEventArgs e)
    {
      if (disabled)
        return;
      CurrentSegmentView.ReCalcConnectedSegments();
    }
    private void ctrlListBoxSrcOrTarget_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (disabled)
        return;
      if (ctrlListBoxSrcOrTarget.SelectedIndex == 0) //MoveSource
      {
        MainWindowParent.MarkerPosition = CurrentSegmentView.SplSegment.SourcePosition;
      }
      else if (ctrlListBoxSrcOrTarget.SelectedIndex == 1)
      {
        MainWindowParent.MarkerPosition = CurrentSegmentView.SplSegment.TargetPosition;
      }
      else if (ctrlListBoxSrcOrTarget.SelectedIndex == 2) //MoveSourceNormal
      {
        MainWindowParent.MarkerPosition = CurrentSegmentView.SplSegment.SourcePosition - (CurrentSegmentView.SplSegment.SourceControlP / SlrrLib.Model.Trc.DividerForBezier);
      }
      else if (ctrlListBoxSrcOrTarget.SelectedIndex == 3)
      {
        MainWindowParent.MarkerPosition = CurrentSegmentView.SplSegment.TargetPosition + (CurrentSegmentView.SplSegment.TargetControlP / SlrrLib.Model.Trc.DividerForBezier);
      }
    }
    private void ctrlButtonBreakSnapping_Click(object sender, RoutedEventArgs e)
    {
      if (ctrlListBoxSrcOrTarget.SelectedIndex == 0) //MoveSource
      {
        CurrentSegmentView.SplSegment.mySrcPointToTheirSrcPoint.Clear();
        CurrentSegmentView.SplSegment.mySrcPointToTheirTarget.Clear();
      }
      else if (ctrlListBoxSrcOrTarget.SelectedIndex == 1)
      {
        CurrentSegmentView.SplSegment.myTargetToTheirSrcPoint.Clear();
        CurrentSegmentView.SplSegment.myTargetToTheirTarget.Clear();
      }
    }
    private void ctrlTextBoxSourceSpeed_KeyUp(object sender, KeyEventArgs e)
    {
      if (disabled)
        return;
      CurrentSegmentView.UpdateRelatedVisuals();
    }
    private void ctrlTextBoxTargetSpeed_KeyUp(object sender, KeyEventArgs e)
    {
      if (disabled)
        return;
      CurrentSegmentView.UpdateRelatedVisuals();
    }
  }
}
