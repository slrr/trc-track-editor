﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TrcTrackEditor.View;

namespace TrcTrackEditor
{
  public partial class CrossingLaneEditor : UserControl
  {
    private bool disabled = true;

    public MainWindow MainWindowParent
    {
      get;
      set;
    }
    public IntersectionSplineSegmentView CurrentSegmentView
    {
      get;
      set;
    }

    public CrossingLaneEditor()
    {
      InitializeComponent();
    }
    public void DisableInteractions()
    {
      disabled = true;
      ctrlTextBoxLanePriority.Text = "";
      ctrlComboBoxLaneType.SelectedIndex = -1;
    }
    public void SetCurrentSegmentView(IntersectionSplineSegmentView newView)
    {
      disabled = false;
      CurrentSegmentView = newView;
      ctrlTextBoxLanePriority.Text = CurrentSegmentView.ManagedLane.LanePriority.ToString();
      switch (CurrentSegmentView.ManagedLane.CrossingDescription)
      {
        case SlrrLib.Model.TrcCrossingType.StraightNonInterfering:
          ctrlComboBoxLaneType.SelectedIndex = 0;
          break;
        case SlrrLib.Model.TrcCrossingType.TurnRightOrNonInterferingTurn:
          ctrlComboBoxLaneType.SelectedIndex = 1;
          break;
        case SlrrLib.Model.TrcCrossingType.InterferingOrLaneCrossingTurn:
          ctrlComboBoxLaneType.SelectedIndex = 2;
          break;
      }
      updateMarkerPosition();
      CurrentSegmentView.UpdateRelatedVisuals();
    }
    public void SubscribeToMarkerMovement()
    {
      MainWindowParent.MarkerPositionChangedByUser += ctrlMainWindowParent_MarkerPositionChangedByUser;
    }

    private void updateMarkerPosition()
    {
      if (disabled)
        return;
      if (ctrlListBoxSrcOrTarget.SelectedIndex == 0) // Move source
      {
        MainWindowParent.MarkerPosition = CurrentSegmentView.SplSegment.SourcePosition;
      }
      else
      {
        MainWindowParent.MarkerPosition = CurrentSegmentView.SplSegment.TargetPosition;
      }
    }

    private void ctrlMainWindowParent_MarkerPositionChangedByUser(object sender, EventArgs e)
    {
      if (disabled)
        return;
      if (ctrlListBoxSrcOrTarget.SelectedIndex == 0) //MoveSource
      {
        CurrentSegmentView.SnapSourceToMarker();
      }
      else if (ctrlListBoxSrcOrTarget.SelectedIndex == 1)
      {
        CurrentSegmentView.SnapTargetToMarker();
      }
      else if (ctrlListBoxSrcOrTarget.SelectedIndex == 2) //MoveSourceNormal
      {
        CurrentSegmentView.SnapSourceControlPToMarker();
      }
      else if (ctrlListBoxSrcOrTarget.SelectedIndex == 3)
      {
        CurrentSegmentView.SnapTargetControlPToMarker();
      }
    }
    private void ctrlButtonSnap_Click(object sender, RoutedEventArgs e)
    {
      if (disabled)
        return;
      if (ctrlListBoxSrcOrTarget.SelectedIndex == 0) //MoveSource
      {
        CurrentSegmentView.SnapSourcePointToFirstClosePoint();
      }
      else if (ctrlListBoxSrcOrTarget.SelectedIndex == 1)
      {
        CurrentSegmentView.SnapTargetPointToFirstClosePoint();
      }
      updateMarkerPosition();
    }
    private void ctrlButtonRecalcSnapping_Click(object sender, RoutedEventArgs e)
    {
      if (disabled)
        return;
      CurrentSegmentView.ReCalcConnectedSegments();
    }
    private void ctrlComboBoxLaneType_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (disabled)
        return;
      if (ctrlComboBoxLaneType.SelectedIndex == 0)
        CurrentSegmentView.ManagedLane.CrossingDescription = SlrrLib.Model.TrcCrossingType.StraightNonInterfering;
      else if (ctrlComboBoxLaneType.SelectedIndex == 1)
        CurrentSegmentView.ManagedLane.CrossingDescription = SlrrLib.Model.TrcCrossingType.TurnRightOrNonInterferingTurn;
      else if (ctrlComboBoxLaneType.SelectedIndex == 2)
        CurrentSegmentView.ManagedLane.CrossingDescription = SlrrLib.Model.TrcCrossingType.InterferingOrLaneCrossingTurn;
    }
    private void ctrlTextBoxLanePriority_KeyUp(object sender, KeyEventArgs e)
    {
      if (disabled)
        return;
      try
      {
        CurrentSegmentView.ManagedLane.LanePriority = short.Parse(ctrlTextBoxLanePriority.Text);
      }
      catch (Exception)
      {
        MessageBox.Show("Bad string for a short: " + ctrlTextBoxLanePriority.Text);
      }
    }
    private void ctrlListBoxSrcOrTarget_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (disabled)
        return;
      if (ctrlListBoxSrcOrTarget.SelectedIndex == 0) //MoveSource
      {
        MainWindowParent.MarkerPosition = CurrentSegmentView.SplSegment.SourcePosition;
      }
      else if (ctrlListBoxSrcOrTarget.SelectedIndex == 1)
      {
        MainWindowParent.MarkerPosition = CurrentSegmentView.SplSegment.TargetPosition;
      }
      else if (ctrlListBoxSrcOrTarget.SelectedIndex == 2) //MoveSourceNormal
      {
        MainWindowParent.MarkerPosition = CurrentSegmentView.SplSegment.SourcePosition - (CurrentSegmentView.SplSegment.SourceControlP / SlrrLib.Model.Trc.DividerForBezier);
      }
      else if (ctrlListBoxSrcOrTarget.SelectedIndex == 3)
      {
        MainWindowParent.MarkerPosition = CurrentSegmentView.SplSegment.TargetPosition + (CurrentSegmentView.SplSegment.TargetControlP / SlrrLib.Model.Trc.DividerForBezier);
      }
    }
    private void ctrlButtonBreakSnapping_Click(object sender, RoutedEventArgs e)
    {
      if (ctrlListBoxSrcOrTarget.SelectedIndex == 0) //MoveSource
      {
        CurrentSegmentView.SplSegment.mySrcPointToTheirSrcPoint.Clear();
        CurrentSegmentView.SplSegment.mySrcPointToTheirTarget.Clear();
      }
      else if (ctrlListBoxSrcOrTarget.SelectedIndex == 1)
      {
        CurrentSegmentView.SplSegment.myTargetToTheirSrcPoint.Clear();
        CurrentSegmentView.SplSegment.myTargetToTheirTarget.Clear();
      }
    }
    private void ctrlTextBoxSourceSpeed_KeyUp(object sender, KeyEventArgs e)
    {
      if (disabled)
        return;
      CurrentSegmentView.UpdateRelatedVisuals();
    }
    private void ctrlTextBoxTargetSpeed_KeyUp(object sender, KeyEventArgs e)
    {
      if (disabled)
        return;
      CurrentSegmentView.UpdateRelatedVisuals();
    }
  }
}
