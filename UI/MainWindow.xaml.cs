﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;
using System.Windows.Media.Animation;
using System.Runtime.InteropServices;

namespace TrcTrackEditor
{
  public partial class MainWindow : Window
  {
    [DllImport("kernel32.dll", ExactSpelling = true)]
    private static extern IntPtr GetConsoleWindow();
    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool SetForegroundWindow(IntPtr hWnd);

    private SlrrLib.Geom.MapRpkModelFactory currentFactory = null;
    private SlrrLib.Model.TrcObject currentClosestTrcStruct = null;
    private string trcFileName = "";
    private IEnumerable<SlrrLib.Geom.NamedModel> spatialData = null;
    private bool selectionLocked = false;

    public event EventHandler MarkerPositionChangedByUser;

    public void OnMarkerPositionChangedByUser()
    {
      if (MarkerPositionChangedByUser != null)
        MarkerPositionChangedByUser.Invoke(this, new EventArgs());
    }
    public SlrrLib.Geom.TrcGeometry TrcGeom
    {
      get;
      private set;
    } = new SlrrLib.Geom.TrcGeometry(new SlrrLib.Model.Trc());
    public Vector3D MarkerPositionInRpkSpace
    {
      get
      {
        return ctrlOrbitingViewport.MarkerPositionInRpkSpatialSpace;
      }
    }
    public Vector3D MarkerPosition
    {
      get
      {
        return ctrlOrbitingViewport.MarkerPosition;
      }
      set
      {
        ctrlOrbitingViewport.LookAtPosition(value);
      }
    }

    public MainWindow()
    {
      Application.Current.DispatcherUnhandledException += current_DispatcherUnhandledException;
      InitializeComponent();
      if (System.IO.File.Exists("lastDir"))
        ctrlOrbitingViewport.LastRpkDirectory = System.IO.File.ReadAllText("lastDir");
      SlrrLib.Model.MessageLog.SetConsoleLogOutput();
      ctrlOrbitingViewport.IsViewDistanceManaged = true;
      ctrlOrbitingViewport.ObjectViewDistance = 1000;
      ctrlOrbitingViewport.MarkerMoved += ctrlOrbitingViewport_MarkerMoved;
      ctrlOrbitingViewport.MarkerMovedByUser += ctrlOrbitingViewport_MarkerMovedByUser;
    }

    public void LockSelection()
    {
      selectionLocked = true;
      ctrlButtonLockSelection.Content = "SelectionIsLocked";
      ctrlButtonLockSelection.Background = Brushes.DarkRed;
    }
    public void UnLockSelection()
    {
      selectionLocked = false;
      ctrlButtonLockSelection.Content = "LockSelection";
      ctrlButtonLockSelection.Background = new SolidColorBrush(Color.FromArgb(0xFF, 0xDD, 0xDD, 0xDD));
    }

    private bool matchingNativeRoad(string rsdStr)
    {
      var spl = rsdStr.Split(new char[] { '\r', '\n', ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
      if (spl.Length >= 2)
      {
        return spl[0].ToLower() == "native" && spl[1].ToLower() == "road";
      }
      return false;
    }
    private void recreateRoadsInRPK()
    {
      SetForegroundWindow(GetConsoleWindow());
      var rpkFact = currentFactory;
      SlrrLib.Model.MessageLog.AddMessage("Creating BinaryRpkData");
      var cityRpk = new SlrrLib.Model.DynamicRpk(rpkFact.RpkLoaded);
      var nativeRoad = cityRpk.Entries.FirstOrDefault(x => x.RSD.InnerEntries.Count == 1
                       && x.RSD.InnerEntries.First() is SlrrLib.Model.DynamicStringInnerEntry
                       && matchingNativeRoad((x.RSD.InnerEntries.First() as SlrrLib.Model.DynamicStringInnerEntry).StringData));
      if (nativeRoad == null)
      {
        nativeRoad = new SlrrLib.Model.DynamicResEntry();
        int freeTypeID = cityRpk.GetFirstFreeTypeIDIncludingHiddenEntries();
        if (freeTypeID == -1)
          throw new Exception("There is no more room in the rpk to add the needed native road ref");
        nativeRoad.SuperID = cityRpk.GetOrAddExternalRefIndexOfRPK("system.rpk") << 16 | 0x00000015;
        nativeRoad.TypeID = freeTypeID;
        nativeRoad.Alias = "road";
        nativeRoad.AdditionalType = 0;
        nativeRoad.IsParentCompatible = 1.0f;
        nativeRoad.TypeOfEntry = 8;
        nativeRoad.RSD = new SlrrLib.Model.DynamicRsdEntry();
        nativeRoad.RSD.InnerEntries.Add(new SlrrLib.Model.DynamicStringInnerEntry("native road"));
        cityRpk.Entries.Add(nativeRoad);
      }
      foreach (var res in cityRpk.Entries)
      {
        if (res.TypeOfEntry == 1 && res.AdditionalType == 17)
        {
          foreach (var rsd in res.RSD.InnerEntries)
          {
            if (rsd is SlrrLib.Model.DynamicRSDInnerEntry)
            {
              var InnerRsd = rsd as SlrrLib.Model.DynamicRSDInnerEntry;
              var lookUp = InnerRsd.GetStringDataAsDict();
              if (lookUp.Any(x => x.Key.ToLower() == "gametype") && lookUp.Any(x => x.Key.ToLower() == "params"))
              {
                var gameTypeLine = lookUp.First(x => x.Key.ToLower() == "gametype");
                var paramsLine = lookUp.First(x => x.Key.ToLower() == "params");
                var splParams = paramsLine.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToList();
                string slrrRootRelativeTrcName = SlrrLib.Model.HighLevel.GameFileManager.GetPathAsSlrrRootRelative(trcFileName, trcFileName).TrimStart('\\', '/');
                string scmFnam = "";
                if (splParams.Count == 0)
                {
                  MessageBox.Show("No TypeID of the ground... no saving done");
                  return;
                }
                if (splParams.Count < 8)
                {
                  MessageBox.Show("No SCM is defined for this rpk that is unusual please choose one..");
                  Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog
                  {
                    InitialDirectory = ctrlOrbitingViewport.LastRpkDirectory,
                    FileName = "",
                    DefaultExt = ".scm",
                    Filter = "scm|*.scm"
                  };
                  var result = dlg.ShowDialog();
                  if (result == true)
                  {
                    scmFnam = dlg.FileName;
                  }
                  else
                  {
                    return;
                  }
                }
                else
                {
                  scmFnam = splParams[7];
                }
                InnerRsd.StringData = gameTypeLine.Key + " \t " + gameTypeLine.Value + "\r\n" +
                                      "params \t " + splParams[0] + ",0,0,0,0,0,0," + scmFnam + "," + slrrRootRelativeTrcName + "\r\n";
              }
            }
          }
        }
        foreach (var rsd in res.RSD.InnerEntries)
        {
          if (rsd is SlrrLib.Model.DynamicSpatialNode)
          {
            var nameArr = rsd as SlrrLib.Model.DynamicSpatialNode;
            nameArr.VisitAllDecendantNamedDatas((SlrrLib.Model.DynamicNamedSpatialData parent, SlrrLib.Model.DynamicNamedSpatialData child) =>
            {
              int gameType = child.GetIntHexValueFromLineNameInFirstRSD("gametype");
              if (gameType == nativeRoad.TypeID)
              {
                if (parent != null)
                  SlrrLib.Model.MessageLog.AddMessage(child.TypeID.ToString("X8") + " V: " + parent.VolumeOfBound().ToString("F4"));
                else
                  SlrrLib.Model.MessageLog.AddMessage(child.TypeID.ToString("X8"));
                if (parent != null)
                  parent.RemoveDirectChild(child);
                else
                  nameArr.RemoveDirectChild(child);
              }
            });
          }
        }
      }
      //rebake trc entries
      var topSpatialStruct = cityRpk.SpatialStructs().First();
      var spatialRes = cityRpk.Entries.First(x => x.RSD.InnerEntries.Any(y => y is SlrrLib.Model.DynamicSpatialNode));
      float spatialMargin = 0.1f;
      float.TryParse(ctrlTextBoxMarginForBaking.Text, out spatialMargin);
      if (spatialMargin < 0.1f)
        spatialMargin = 0.1f;
      TrcGeom.Trc.FillIntersectionRefIndices();
      TrcGeom.Trc.FillSplineLengthsInRegardToMaxSpeed();
      TrcGeom.Trc.RegenerateRaceEntries();
      var TypeIDs = cityRpk.GetFreeTypeIDsIncludingHiddenEntries(TrcGeom.Trc.IntersectionData1.Count +
                    TrcGeom.Trc.IntersectionData2.Count + TrcGeom.Trc.RaceNodeDescriptors.Count);
      int freeTypeID_i = 0;
      foreach (var cross in TrcGeom.Trc.IntersectionData1)
      {
        var toad = TrcGeom.Trc.GenNamedDataForIntersection(cross, nativeRoad, TypeIDs[freeTypeID_i], spatialMargin);
        freeTypeID_i++;
        toad.SuperID = spatialRes.TypeID;
        var newParent = topSpatialStruct.PushDownDeepNameEntryWithBound(toad);
        if (newParent != null)
          SlrrLib.Model.MessageLog.AddMessage("Adding IntersectionData1 with 0x" + toad.TypeID.ToString("X8") + " V: " + newParent.VolumeOfBound().ToString("F4"));
        else
          SlrrLib.Model.MessageLog.AddError("Bound not found for 0x" + toad.TypeID.ToString("X8"));
      }
      foreach (var cross in TrcGeom.Trc.IntersectionData2)
      {
        var toad = TrcGeom.Trc.GenNamedDataForIntersection(cross, nativeRoad, TypeIDs[freeTypeID_i], spatialMargin);
        freeTypeID_i++;
        toad.SuperID = spatialRes.TypeID;
        var newParent = topSpatialStruct.PushDownDeepNameEntryWithBound(toad);
        if (newParent != null)
          SlrrLib.Model.MessageLog.AddMessage("Adding IntersectionData2 with 0x" + toad.TypeID.ToString("X8") + " V: " + newParent.VolumeOfBound().ToString("F4"));
        else
          SlrrLib.Model.MessageLog.AddError("Bound not found for 0x" + toad.TypeID.ToString("X8"));
      }
      foreach (var race in TrcGeom.Trc.RaceNodeDescriptors)
      {
        var toad = TrcGeom.Trc.GenNamedDataForRaceNode(race, nativeRoad, TypeIDs[freeTypeID_i], spatialMargin);
        freeTypeID_i++;
        toad.SuperID = spatialRes.TypeID;
        var newParent = topSpatialStruct.PushDownDeepNameEntryWithBound(toad);
        if (newParent != null)
          SlrrLib.Model.MessageLog.AddMessage("Adding RaceNodeDescriptors with 0x" + toad.TypeID.ToString("X8") + " V: " + newParent.VolumeOfBound().ToString("F4"));
        else
          SlrrLib.Model.MessageLog.AddError("Bound not found for 0x" + toad.TypeID.ToString("X8"));
      }
      SlrrLib.Model.MessageLog.AddMessage("Saving.. " + currentFactory.LoadedRpkName);
      cityRpk.SaveAs(currentFactory.LoadedRpkName);
      SlrrLib.Model.MessageLog.AddMessage("Done");
    }
    private void manageTrcStructMouseMove()
    {
      if (TrcGeom != null && !selectionLocked)
      {
        var ClosestTrcObj = TrcGeom.GetClosestEntity(ctrlOrbitingViewport.MarkerPositionInRpkSpatialSpace);
        if (ClosestTrcObj != currentClosestTrcStruct)
        {
          TrcGeom.HighLightChildrenEntititesClearBefore(ClosestTrcObj);
          currentClosestTrcStruct = ClosestTrcObj;
          ctrlIntersectionEditor.ClearSelection();
          ctrlIntersectionEditor.ClearListbox();
          ctrlLaneEditor.ClearSelection();
          ctrlLaneEditor.ClearListbox();
          if (ClosestTrcObj is SlrrLib.Model.TrcIntersection)
          {
            ctrlIntersectionEditor.ConnectIntersection(ClosestTrcObj as SlrrLib.Model.TrcIntersection, TrcGeom.Trc, this);
            ctrlRowDefinitionTrcIntersectionEditor.Height = new GridLength(1, GridUnitType.Star);
            ctrlRowDefinitionTrcLaneEditor.Height = new GridLength(0, GridUnitType.Pixel);
          }
          else if (ClosestTrcObj is SlrrLib.Model.TrcLane)
          {
            ctrlLaneEditor.ConnectLane(ClosestTrcObj as SlrrLib.Model.TrcLane, TrcGeom.Trc, this);
            ctrlRowDefinitionTrcLaneEditor.Height = new GridLength(1, GridUnitType.Star);
            ctrlRowDefinitionTrcIntersectionEditor.Height = new GridLength(0, GridUnitType.Pixel);
          }
        }
      }
    }
    private void removeSpatialDataStructVisualisation()
    {
      if (spatialData == null)
        return;
      foreach (var model in spatialData)
        ctrlOrbitingViewport.RemoveModelFromScene(model);
    }
    private void addSpatialDataStructVisualisation()
    {
      removeSpatialDataStructVisualisation();
      spatialData = currentFactory.GetSpatialRepresentationForPoint(ctrlOrbitingViewport.MarkerPositionInRpkSpatialSpace).Select(x => new SlrrLib.Geom.NamedModel
      {
        ModelGeom = x,
        Name = "Spatial struct box",
        Translate = new Vector3D(x.Bounds.X, x.Bounds.Y, x.Bounds.Z)
      }).ToList();
      foreach (var model in spatialData)
        ctrlOrbitingViewport.AddModelToScene(model);
    }

    private void current_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
    {
      System.IO.File.WriteAllText("UnhandledException.txt", e.Exception.Message + "\r\n\r\n\r\n" + e.Exception.StackTrace);
    }
    private void ctrlOrbitingViewport_MarkerMoved(object sender, EventArgs e)
    {
      manageTrcStructMouseMove();
    }
    private void ctrlOrbitingViewport_MarkerMovedByUser(object sender, EventArgs e)
    {
      OnMarkerPositionChangedByUser();
      manageTrcStructMouseMove();
    }
    private void ctrlButtonAddTrc_Click(object sender, RoutedEventArgs e)
    {
      Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog
      {
        InitialDirectory = ctrlOrbitingViewport.LastRpkDirectory,
        FileName = "",
        DefaultExt = ".trc",
        Filter = "trc|*.trc"
      };

      var result = dlg.ShowDialog();
      if (result == true)
      {
        trcFileName = dlg.FileName;
        TrcGeom = new SlrrLib.Geom.TrcGeometry(SlrrLib.Model.Trc.Load(trcFileName));
        TrcGeom.GenerateVisuals();
        foreach (var trcObj in TrcGeom.TrcStructToGeom)
        {
          ctrlOrbitingViewport.Add3DRepresentationToScene(trcObj.Value, "trc-obj");
        }
        ctrlOrbitingViewport.UpdateVisibleObjectsViewDistance();
      }
    }
    private void ctrlButtonClearTrc_Click(object sender, RoutedEventArgs e)
    {
      foreach (var trcObj in TrcGeom.TrcStructToGeom)
      {
        ctrlOrbitingViewport.RemoveAllModelsBy3DRepresentation(trcObj.Value);
      }
      TrcGeom = new SlrrLib.Geom.TrcGeometry(new SlrrLib.Model.Trc());
      ctrlOrbitingViewport.UpdateVisibleObjectsViewDistance();
    }
    private void ctrlButtonSaveTrc_Click(object sender, RoutedEventArgs e)
    {
      var dlg = new Microsoft.Win32.SaveFileDialog
      {
        InitialDirectory = ctrlOrbitingViewport.LastRpkDirectory,
        FileName = trcFileName,
        DefaultExt = ".trc",
        Filter = "trcs|*.trc"
      };
      var result = dlg.ShowDialog();
      if (result == true)
      {
        try
        {
          trcFileName = dlg.FileName;
          recreateRoadsInRPK();
          TrcGeom.Trc.Write(trcFileName);
        }
        catch (Exception ex)
        {
          MessageBox.Show(ex.Message);
        }
      }
    }
    private void ctrlButtonRenderScene_Click(object sender, RoutedEventArgs e)
    {
      currentFactory = ctrlOrbitingViewport.LoadSceneRpk();
      if(currentFactory == null)
        return;
      Title = ctrlOrbitingViewport.LastRpkOpened;
    }
    private void ctrlButtonAddToScene_Click(object sender, RoutedEventArgs e)
    {
      ctrlOrbitingViewport.LoadSecondaryRpk();
    }
    private void ctrlButtonNewIntersection1_Click(object sender, RoutedEventArgs e)
    {
      SlrrLib.Model.TrcIntersection cross = new SlrrLib.Model.TrcIntersection();
      SlrrLib.Model.TrcIntersectionLane crossLane = new SlrrLib.Model.TrcIntersectionLane
      {
        LanePriority = 10
      };
      SlrrLib.Model.TrcSplineSegment segment = new SlrrLib.Model.TrcSplineSegment
      {
        SourceLanePosition = ctrlOrbitingViewport.MarkerPositionInRpkSpatialSpace
      };
      segment.TargetLanePosition = segment.SourceLanePosition - new Vector3D(10, 0, 0);
      segment.LengthOfSplineAtTarget = 10;
      segment.LengthOfSplineAtSource = 10;
      segment.SourceLaneMinusDeltaControlP = new Vector3D(1, 0, 0);
      segment.TargetLaneDeltaControlPoint = new Vector3D(1, 0, 0);
      segment.UnkByte_1 = 1;
      crossLane.LaneShape = segment;
      cross.Crossings.Add(crossLane);
      TrcGeom.Trc.IntersectionData1.Add(cross);
      var toad = TrcGeom.MeshFromIntersection(cross, TrcGeom.Intersection1Color, TrcGeom.CrossThickness);
      ctrlOrbitingViewport.ReplaceAll3DRepresentations(TrcGeom.TrcStructToGeom[cross], toad);
      TrcGeom.TrcStructToGeom[cross] = toad;
      UnLockSelection();
      manageTrcStructMouseMove();
    }
    private void ctrlButtonNewIntersection2_Click(object sender, RoutedEventArgs e)
    {
      SlrrLib.Model.TrcIntersection cross = new SlrrLib.Model.TrcIntersection();
      SlrrLib.Model.TrcIntersectionLane crossLane = new SlrrLib.Model.TrcIntersectionLane
      {
        LanePriority = 10
      };
      SlrrLib.Model.TrcSplineSegment segment = new SlrrLib.Model.TrcSplineSegment
      {
        SourceLanePosition = ctrlOrbitingViewport.MarkerPositionInRpkSpatialSpace
      };
      segment.TargetLanePosition = segment.SourceLanePosition - new Vector3D(10, 0, 0);
      segment.LengthOfSplineAtTarget = 10;
      segment.LengthOfSplineAtSource = 10;
      segment.SourceLaneMinusDeltaControlP = new Vector3D(1, 0, 0);
      segment.TargetLaneDeltaControlPoint = new Vector3D(1, 0, 0);
      segment.UnkByte_1 = 1;
      crossLane.LaneShape = segment;
      cross.Crossings.Add(crossLane);
      TrcGeom.Trc.IntersectionData2.Add(cross);
      var toad = TrcGeom.MeshFromIntersection(cross, TrcGeom.Intersection2Color, TrcGeom.CrossThickness);
      ctrlOrbitingViewport.ReplaceAll3DRepresentations(TrcGeom.TrcStructToGeom[cross], toad);
      TrcGeom.TrcStructToGeom[cross] = toad;
      UnLockSelection();
      manageTrcStructMouseMove();
    }
    private void ctrlButtonNewLane_Click(object sender, RoutedEventArgs e)
    {
      SlrrLib.Model.TrcLane lane = new SlrrLib.Model.TrcLane();
      SlrrLib.Model.TrcSplineSegment segment = new SlrrLib.Model.TrcSplineSegment
      {
        SourceLanePosition = ctrlOrbitingViewport.MarkerPositionInRpkSpatialSpace
      };
      segment.TargetLanePosition = segment.SourceLanePosition - new Vector3D(10, 0, 0);
      segment.LengthOfSplineAtTarget = 10;
      segment.LengthOfSplineAtSource = 10;
      segment.SourceLaneMinusDeltaControlP = new Vector3D(1, 0, 0);
      segment.TargetLaneDeltaControlPoint = new Vector3D(1, 0, 0);
      segment.UnkByte_1 = 1;
      lane.Spline.Add(segment);
      lane.MaxSpeed = 13.8889f;
      lane.EndFloatData_2 = 0.9f;
      lane.MinusOne = -1;
      TrcGeom.Trc.LaneData.Add(lane);
      var toad = TrcGeom.MeshFromLane(lane, TrcGeom.LaneColor, TrcGeom.LaneThickness);
      ctrlOrbitingViewport.ReplaceAll3DRepresentations(TrcGeom.TrcStructToGeom[lane], toad);
      TrcGeom.TrcStructToGeom[lane] = toad;
      UnLockSelection();
      manageTrcStructMouseMove();
    }
    private void ctrlButtonBakeTrc_Click(object sender, RoutedEventArgs e)
    {
      recreateRoadsInRPK();
    }
    private void ctrlButtonResolveSpatialStruct_Click(object sender, RoutedEventArgs e)
    {
      addSpatialDataStructVisualisation();
    }
    private void ctrlButtonDeleteSpatialStruct_Click(object sender, RoutedEventArgs e)
    {
      removeSpatialDataStructVisualisation();
    }
    private void ctrlButtonLockSelection_Click(object sender, RoutedEventArgs e)
    {
      selectionLocked = !selectionLocked;
      if (selectionLocked)
      {
        LockSelection();
      }
      else
      {
        UnLockSelection();
        ctrlIntersectionEditor.ClearSelection();
        ctrlLaneEditor.ClearSelection();
      }
    }
    private void ctrlButtonSaveOnlyTrc_Click(object sender, RoutedEventArgs e)
    {
      var dlg = new Microsoft.Win32.SaveFileDialog
      {
        InitialDirectory = ctrlOrbitingViewport.LastRpkDirectory,
        FileName = trcFileName,
        DefaultExt = ".trc",
        Filter = "trcs|*.trc"
      };
      var result = dlg.ShowDialog();
      if (result == true)
      {
        try
        {
          trcFileName = dlg.FileName;
          TrcGeom.Trc.FillIntersectionRefIndices();
          TrcGeom.Trc.RegenerateRaceEntries();
          TrcGeom.Trc.Write(trcFileName);
        }
        catch (Exception ex)
        {
          MessageBox.Show(ex.Message);
        }
      }
    }
    private void ctrlButtonDelSelected_Click(object sender, RoutedEventArgs e)
    {
      if (currentClosestTrcStruct == null)
        return;
      TrcGeom.ClearHighLight();
      ctrlOrbitingViewport.RemoveAllModelsBy3DRepresentation(TrcGeom.TrcStructToGeom[currentClosestTrcStruct]);
      TrcGeom.RemoveTrcObject(currentClosestTrcStruct);
    }
    private void ctrlButtonDupLane_Click(object sender, RoutedEventArgs e)
    {
      if (currentClosestTrcStruct is SlrrLib.Model.TrcLane)
      {
        var cur = currentClosestTrcStruct as SlrrLib.Model.TrcLane;
        SlrrLib.Model.TrcLane cpy = new SlrrLib.Model.TrcLane
        {
          EndFloatData_2 = cur.EndFloatData_2,
          MinXOfPseudoSelf = cur.MinXOfPseudoSelf,
          MaxXOfPseudoSelf = cur.MaxXOfPseudoSelf,
          MaxxTrueOfPseudoSelf = cur.MaxxTrueOfPseudoSelf,
          MaxSpeed = cur.MaxSpeed,
          MinusOne = -1,
          Pos1X = cur.Pos1X,
          Pos1Y = cur.Pos1Y,
          Pos1Z = cur.Pos1Z,
          Pos2X = cur.Pos2X,
          Pos2Y = cur.Pos2Y,
          Pos2Z = cur.Pos2Z,
          Spline = new List<SlrrLib.Model.TrcSplineSegment>()
        };
        foreach (var flt in cur.Spline)
        {
          SlrrLib.Model.TrcSplineSegment toad = new SlrrLib.Model.TrcSplineSegment
          {
            SourceLaneMinusDeltaControlP = flt.SourceLaneMinusDeltaControlP,
            TargetLaneDeltaControlPoint = flt.TargetLaneDeltaControlPoint,
            SourceLanePosition = flt.SourceLanePosition + new Vector3D(1, 0, 0),
            TargetLanePosition = flt.TargetLanePosition + new Vector3D(1, 0, 0)
          };
          cpy.Spline.Add(toad);
        }
        TrcGeom.Trc.LaneData.Add(cpy);
        var geom = TrcGeom.MeshFromLane(cpy, TrcGeom.LaneColor, TrcGeom.LaneThickness);
        TrcGeom.TrcStructToGeom[cpy] = geom;
        ctrlOrbitingViewport.Add3DRepresentationToScene(TrcGeom.TrcStructToGeom[cpy]);
        UnLockSelection();
        manageTrcStructMouseMove();
      }
    }
    private void ctrlButtonGenerateAllAdjacency_Click(object sender, RoutedEventArgs e)
    {
      int i = 0;
      int max = TrcGeom.Trc.LaneData.Count;
      SetForegroundWindow(GetConsoleWindow());
      foreach (var lane in TrcGeom.Trc.LaneData)
      {
        i++;
        SlrrLib.Model.MessageLog.AddMessage("Proc lane " + i.ToString("D4") + "\\" + max.ToString());
        TrcGeom.Trc.RecalcConnectionsForLane(lane);
        TrcGeom.Trc.GenerateAdjacentLanesForLane(lane);
      }
      SlrrLib.Model.MessageLog.AddMessage("Done");
    }
  }
}
